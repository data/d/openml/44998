# OpenML dataset: Airlines_DepDelay_1M

https://www.openml.org/d/44998

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The dataset contains information about departure delays of airlines from years 1987 - 2013.

This is a subset of the 10M version (which is once again a subset of the original dataset).

Unique carrier is the airline: https://aspm.faa.gov/aspmhelp/index/ASQP__Carrier_Codes_and_Names.html

Preprocessing on time features of the data has been performed.

**Attribute Description**

1. *DepDelay* - target feature, how long the delay was
2. *Month*
3. *DayofMonth* - 1-31
4. *DayOfWeek* - 1 (Monday) - 7 (Sunday)
5. *UniqueCarrier* - unique carrier code
6. *Origin* - origin airport code
7. *Dest* - destination airport code
8. *Distance* - distance between the airports in miles
9. *CRSDepTime_hour*  - scheduled departure time, hour
10. *CRSDepTime_minute* - scheduled departure time, minutes
11. *CRSArrTime_hour* - scheduled arrive time, hour
12. *CRSArrTime_minute* - schedule arrive time, minutes

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44998) of an [OpenML dataset](https://www.openml.org/d/44998). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44998/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44998/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44998/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

